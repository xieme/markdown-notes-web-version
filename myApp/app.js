const koa = require('koa');
const app = new koa();
const router =require('./router/router')
const static = require('koa-static');
const bodyParser = require('koa-bodyparser');
const path = require('path')
require('./utils/dateFormat');
const templating = require('./utils/templating')


app.use(static(path.join(__dirname, '/public')))

app.use(bodyParser())

app.use(templating)

app.use(router.routes());





app.listen(3000)
console.log('http://localhost:3000');