const multer = require('koa-multer');
const path = require('path')

let storage = multer.diskStorage({
    destination: path.resolve(__dirname, '../', 'note'),
    filename: (ctx, file, cb) => {
        cb(null, file.originalname);
    }
});


let fileFilter = (ctx, file, cb) => {
    //上传的后缀为md的文件
    let str = file.originalname

    if (str.substring(str.lastIndexOf('.') + 1) === 'md') {
        cb(null, true);

    } else {
        cb(null, false);
    }
}

let upload = multer({ storage: storage, fileFilter: fileFilter });



module.exports = upload;