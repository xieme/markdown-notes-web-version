const multer = require('koa-multer');
const path = require('path')

let storage = multer.diskStorage({
    destination: path.resolve(__dirname, '../', 'public/imgs'),
    filename: (ctx, file, cb) => {
        cb(null, file.originalname);
    }
});

let fileFilter = (ctx, file, cb) => {
   

    let str = file.originalname
    let types = ['jpg', 'jpeg', 'png', 'gif'];
    let temType = str.substring(str.lastIndexOf('.') + 1);

    if (types.indexOf(temType) !== -1) {
        cb(null, true);

    } else {
        cb(null, false);
    }
}
let uploadImg = multer({ storage: storage, fileFilter: fileFilter });

module.exports = uploadImg